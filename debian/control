Source: spfft
Section: libs
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Andrius Merkys <merkys@debian.org>,
Build-Depends:
 chrpath,
 cmake,
 debhelper-compat (= 13),
 libfftw3-dev,
 mpi-default-dev,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://github.com/eth-cscs/SpFFT
Vcs-Browser: https://salsa.debian.org/science-team/spfft
Vcs-Git: https://salsa.debian.org/science-team/spfft.git

Package: libspfft-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libspfft1 (= ${binary:Version}),
 ${misc:Depends},
Description: Sparse 3D FFT library with MPI, OpenMP, CUDA / ROCm support (development files)
 SpFFT was originally intended for transforms of data with spherical cutoff in
 frequency domain, as required by some computational material science codes.
 For distributed computations, SpFFT uses a slab decomposition in space domain
 and pencil decomposition in frequency domain (all sparse data within a pencil
 must be on one rank). If desired, the library can be compiled without any
 parallelization (MPI, OpenMP, CUDA / ROCm).
 .
 This package contains development files.

Package: libspfft1
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Sparse 3D FFT library with MPI, OpenMP, CUDA / ROCm support
 SpFFT was originally intended for transforms of data with spherical cutoff in
 frequency domain, as required by some computational material science codes.
 For distributed computations, SpFFT uses a slab decomposition in space domain
 and pencil decomposition in frequency domain (all sparse data within a pencil
 must be on one rank). If desired, the library can be compiled without any
 parallelization (MPI, OpenMP, CUDA / ROCm).
